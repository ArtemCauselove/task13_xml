<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table.tfmt {
                    border: 1px ;
                    }

                    td.colfmt {
                    border: 1px ;
                    background-color: yellow;
                    color: red;
                    text-align:right;
                    }

                    th {
                    background-color: #2E6AFE;
                    color: white;
                    }

                </style>
            </head>

            <body>
                <table class="tfmt">
                    <tr>
                        <th style="width:250px">Name</th>
                        <th style="width:350px">type</th>
                        <th style="width:250px">production</th>
                        <th style="width:250px">ingredients</th>
                        <th style="width:250px">weight</th>
                        <th style="width:250px">transFat</th>
                        <th style="width:250px">vegan</th>
                        <th style="width:250px">nutritions</th>
                        <th style="width:250px">cookMethod</th>
                        <th style="width:250px">wrapMaterial</th>

                    </tr>
                    <xsl:for-each select="candies/candie">
                        <tr>
                            <td class="colfmt">
                                <xsl:value-of select="@candieNo"/>
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="name" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="type" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="production" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="ingredient" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="weight" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="protein" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="fat" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="carbohydrates" />
                            </td>
                            <td class="colfmt">
                                <xsl:value-of select="transFat" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="vegan" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="energy" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="cookMethod" />
                            </td>

                            <td class="colfmt">
                                <xsl:value-of select="wrapMaterial" />
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>