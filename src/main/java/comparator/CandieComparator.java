package comparator;

import model.Candie;

import java.util.Comparator;

public class CandieComparator implements Comparator<Candie> {
    @Override
    public int compare(Candie o1, Candie o2) {
        return Double.compare(o1.getValue().getEnergy(), o2.getValue().getEnergy());
    }
}
