package model;

public class Value {
    private double weight;
    private double carbohydrates;
    private double protein;
    private double fat;
    private double transFat;
    private int energy;
    private CookMethod cookMethod;

    public Value() {
    }

    public Value(double weight, double carbohydrates, double protein, double fat, double transFat, int energy, CookMethod cookMethod) {
        this.weight = weight;
        this.carbohydrates = carbohydrates;
        this.protein = protein;
        this.fat = fat;
        this.transFat = transFat;
        this.energy = energy;
        this.cookMethod = cookMethod;
    }


    @Override
    public String toString() {
        return "Value{" +
                "weight=" + weight +
                ", carbohydrates=" + carbohydrates +
                ", protein=" + protein +
                ", fat=" + fat +
                ", transFat=" + transFat +
                ", energy=" + energy +
                ", CookMethod=" + cookMethod +
                '}';
    }


    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public double getTransFat() {
        return transFat;
    }

    public void setTransFat(double transFat) {
        this.transFat = transFat;
    }

    public CookMethod getCookMethod() {
        return cookMethod;
    }

    public void setCookMethod(CookMethod cookMethod) {
        this.cookMethod = cookMethod;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }
}

