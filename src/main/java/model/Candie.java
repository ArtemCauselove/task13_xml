package model;

import java.util.ArrayList;
import java.util.List;

public class Candie {
    private int candieNo;
    private String name;
    private String type;
    private boolean vegan;
    private String production;
    private List<Ingredient> ingredients = new ArrayList<>();
    private Value value = new Value();

    public Candie() {
    }

    public Candie(int candieNo, String name, String type, boolean vegan, String production, List<Ingredient> ingredients, Value value) {
        this.candieNo = candieNo;
        this.name = name;
        this.type = type;
        this.vegan = vegan;
        this.production = production;
        this.ingredients = ingredients;
        this.value = value;
    }

    public int getCandieNo() {
        return candieNo;
    }

    public void setCandieNo(int candieNo) {
        this.candieNo = candieNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isVegan() {
        return vegan;
    }

    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

   @Override
    public String toString() {
        return "Candie{" +
                "beerNo=" + candieNo +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", vegan='" + vegan +
                ", production='" + production + '\'' +
                ", ingredients=" + ingredients +
                ", chars=" + value +
                '}';
    }
}
