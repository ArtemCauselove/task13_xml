package model;

public class CookMethod {
    private String technology;
    private String material;

    public CookMethod() {}

    public CookMethod(String technology, String material) {
        this.technology = technology;
        this.material = material;
    }

    public String getTechnology() {
        return technology;
    }

    public String getMaterial() {
        return material;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "CookMethod{" +
                "technology=" + technology +
                ", WrapMaterial='" + material + '\'' +
                '}';
    }
}
