package parser;


import comparator.CandieComparator;
import filechecker.ExtensionChecker;
import model.Candie;
import parser.dom.DOMParserUser;
import parser.sax.SAXParserUser;
import parser.stax.StAXReader;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Parser {

  public static void main(String[] args) {
    File xml = new File("src\\main\\resources\\xml\\candieXML.xml");
    File xsd = new File("src\\main\\resources\\xml\\candiesXSD.xsd");

    if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            SAX
      printList(SAXParserUser.parseCandies(xml, xsd), "SAX");

//            StAX
      printList(StAXReader.parseCandies(xml, xsd), "StAX");

//            DOM
      printList(DOMParserUser.getCandieList(xml, xsd), "DOM");
    }
  }

  private static boolean checkIfXSD(File xsd) {
    return ExtensionChecker.isXSD(xsd);
  }

  private static boolean checkIfXML(File xml) {
    return ExtensionChecker.isXML(xml);
  }

  private static void printList(List<Candie> candies, String parserName) {
    Collections.sort(candies, new CandieComparator());
    System.out.println(parserName);
    for (Candie candie : candies) {
      System.out.println(candie);
    }
  }

}
