package parser.stax;

import model.Candie;
import model.CookMethod;
import model.Value;
import model.Ingredient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StAXReader {
    public static List<Candie> parseCandies(File xml, File xsd){
        List<Candie> candieList = new ArrayList<>();
        Candie candie = null;
        Value value = null;
        CookMethod cookMethod = null;
        List<Ingredient> ingredients = null;
        Ingredient ingridient = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
            while(xmlEventReader.hasNext()){
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()){
                    StartElement startElement = xmlEvent.asStartElement();
                    String name = startElement.getName().getLocalPart();
                    switch (name) {
                        case "candie":
                            candie = new Candie();

                            Attribute idAttr = startElement.getAttributeByName(new QName("candieNo"));
                            if (idAttr != null) {
                                candie.setCandieNo(Integer.parseInt(idAttr.getValue()));
                            }
                            break;
                        case "name":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candie != null;
                            candie.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "type":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candie != null;
                            candie.setType(xmlEvent.asCharacters().getData());
                            break;
                        case "vegan":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candie != null;
                            candie.setVegan(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
                            break;
                        case "production":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert candie != null;
                            candie.setProduction(xmlEvent.asCharacters().getData());
                            break;
                        case "ingredients":
                            xmlEvent = xmlEventReader.nextEvent();
                            ingredients = new ArrayList<>();
                            break;
                        case "ingredient":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert ingredients != null;
                            ingredients.add(new Ingredient(xmlEvent.asCharacters().getData()));
                            break;
                        case "value":
                            xmlEvent = xmlEventReader.nextEvent();
                            value = new Value();
                            break;
                        case "weight":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setWeight(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "carbohydrates":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setCarbohydrates(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "protein":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setProtein(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "fat":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setFat(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "transFat":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setTransFat(Double.parseDouble(xmlEvent.asCharacters().getData()));
                            break;
                        case "energy":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert value != null;
                            value.setEnergy(Integer.parseInt(xmlEvent.asCharacters().getData()));
                            break;
                        case "cookMethod":
                            xmlEvent = xmlEventReader.nextEvent();
                            cookMethod = new CookMethod();
                            break;
                        case "technology":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert cookMethod != null;
                            cookMethod.setTechnology(xmlEvent.asCharacters().getData());
                            break;

                        case "tankMaterial":
                            xmlEvent = xmlEventReader.nextEvent();
                            assert cookMethod != null;
                            cookMethod.setMaterial(xmlEvent.asCharacters().getData());
                            assert value != null;
                            value.setCookMethod(cookMethod);
                            assert candie != null;
                            candie.setValue(value);
                            candie.setIngredients(ingredients);
                            break;
                    }
                }

                if(xmlEvent.isEndElement()){
                    EndElement endElement = xmlEvent.asEndElement();
                    if(endElement.getName().getLocalPart().equals("candie")){
                        candieList.add(candie);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return candieList;
    }
}
