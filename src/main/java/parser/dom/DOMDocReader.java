package parser.dom;

import model.Candie;
import model.Value;
import model.Ingredient;
import model.CookMethod;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMDocReader {
    public List<Candie> readDoc(Document doc){
        doc.getDocumentElement().normalize();
        List<Candie> candies = new ArrayList<>();

        NodeList nodeList = doc.getElementsByTagName("candie");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Candie candie = new Candie();
            Value value;
            List<Ingredient> ingredients;
            CookMethod cookMethod;

            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE){
                Element element = (Element) node;
                candie.setName(element.getElementsByTagName("name").item(0).getTextContent());
                candie.setType(element.getElementsByTagName("type").item(0).getTextContent());
                candie.setVegan(Boolean.parseBoolean(element.getElementsByTagName("vegan").item(0).getTextContent()));
                candie.setProduction(element.getElementsByTagName("production").item(0).getTextContent());

                ingredients = getIngredients(element.getElementsByTagName("ingredients"));
                value = getValues(element.getElementsByTagName("value"));
                cookMethod = getCookMethod(element.getElementsByTagName("cookMethod"));

                value.setCookMethod(cookMethod);
                candie.setValue(value);
                candie.setIngredients(ingredients);
                candies.add(candie);
            }
        }
        return candies;
    }

    private List<Ingredient> getIngredients(NodeList nodes){
        List<Ingredient> ingredients = new ArrayList<>();
        if(nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            NodeList nodeList = element.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE){
                    Element el = (Element) node;
                    ingredients.add(new Ingredient(el.getTextContent()));
                }
            }
        }

        return ingredients;
    }

    private Value getValues(NodeList nodes){
        Value chars = new Value();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            chars.setWeight(Double.parseDouble(element.getElementsByTagName("weight").item(0).getTextContent()));
            chars.setCarbohydrates(Double.parseDouble(element.getElementsByTagName("carbohydrates").item(0).getTextContent()));
            chars.setProtein(Double.parseDouble(element.getElementsByTagName("protein").item(0).getTextContent()));
            chars.setFat(Double.parseDouble(element.getElementsByTagName("fat").item(0).getTextContent()));
            chars.setTransFat(Double.parseDouble(element.getElementsByTagName("transFat").item(0).getTextContent()));
            chars.setEnergy(Integer.parseInt(element.getElementsByTagName("energy").item(0).getTextContent()));
        }

        return chars;
    }

    private CookMethod getCookMethod(NodeList nodes){
        CookMethod spillMethod = new CookMethod();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            spillMethod.setTechnology((element.getElementsByTagName("technology").item(0).getTextContent()));
            spillMethod.setMaterial(element.getElementsByTagName("wrapMaterial").item(0).getTextContent());
        }
        return spillMethod;
    }
}
