package parser.dom;

import jdk.internal.org.xml.sax.SAXException;
import model.Candie;
import org.w3c.dom.Document;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DOMParserUser {
    public static List<Candie> getCandieList(File xml, File xsd){
        DOMDocCreator creator = new DOMDocCreator(xml);
        Document doc = creator.getDocument();

/*
        try {
            DOMValidator.validate(DOMValidator.createSchema(xsd),doc);
        }catch (IOException | org.xml.sax.SAXException ex){
            ex.printStackTrace();
        }
*/

        DOMDocReader reader = new DOMDocReader();

        return reader.readDoc(doc);
    }
}
