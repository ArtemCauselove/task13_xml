package parser.sax;


import model.Candie;
import model.CookMethod;
import model.Value;
import model.Ingredient;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
    private List<Candie> candieList = new ArrayList<>();
    private Candie candie = null;
    private List<Ingredient> ingredients = null;
    private Value value = null;
    private CookMethod cookMethod = null;
    private boolean cName = false;
    private boolean cType = false;
    private boolean cVeg = false;
    private boolean cProd = false;
    private boolean cIngreds = false;
    private boolean cIngred = false;
    private boolean cChoco = false;
    private boolean cValues = false;
    private boolean cWeig = false;
    private boolean cCarbs = false;
    private boolean cProt = false;
    private boolean cNutr = false;
    private boolean cFat = false;
    private boolean cTransFat = false;
    private boolean cEnerg = false;
    private boolean cTech = false;
    private boolean cMater = false;

    public List<Candie> getCandieList(){
        return this.candieList;
    }

    public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("candie")){
            String beerN = attributes.getValue("candieNo");
            candie = new Candie();
            candie.setCandieNo(Integer.parseInt(beerN));
        }
        else if (qName.equalsIgnoreCase("name")){cName = true;}
        else if (qName.equalsIgnoreCase("type")){cType = true;}
        else if (qName.equalsIgnoreCase("vegan")){cVeg = true;}
        else if (qName.equalsIgnoreCase("production")){cProd = true;}
        else if (qName.equalsIgnoreCase("ingredients")){cIngreds = true;}
        else if (qName.equalsIgnoreCase("ingredient")){cIngred = true;}
        else if (qName.equalsIgnoreCase("chocolate")){cChoco = true;}
        else if (qName.equalsIgnoreCase("value")){cValues = true;}
        else if (qName.equalsIgnoreCase("weight")){cWeig = true;}
        else if (qName.equalsIgnoreCase("carbohydrates")){cCarbs = true;}
        else if (qName.equalsIgnoreCase("protein")){cProt = true;}
        else if (qName.equalsIgnoreCase("nutritions")){cNutr = true;}
        else if (qName.equalsIgnoreCase("fat")){cFat = true;}
        else if (qName.equalsIgnoreCase("transFat")){cTransFat = true;}
        else if (qName.equalsIgnoreCase("energy")){cEnerg = true;}
        else if (qName.equalsIgnoreCase("technology")){cTech = true;}
        else if (qName.equalsIgnoreCase("wrapMaterial")){cMater = true;}
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("candie")){
            candieList.add(candie);
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (cName){
            candie.setName(new String(ch, start, length));
            cName = false;
        }
        else if (cType){
            candie.setType(new String(ch, start, length));
            cType = false;
        }
        else if (cVeg){
            candie.setVegan(Boolean.parseBoolean(new String(ch, start, length)));
            cVeg = false;
        }
        else if (cProd){
            String prod = new String(ch, start, length);
            candie.setProduction(prod);
            cProd = false;
        }
        else if(cIngreds){
            ingredients = new ArrayList<>();
            cIngreds = false;
        }
        else if (cIngred){
            Ingredient ingredient = new Ingredient();
            ingredient.setName(new String(ch, start, length));
            ingredients.add(ingredient);
            cIngred = false;
        }
        else if (cValues){
            value = new Value();
            cValues = false;
        }
        else if (cWeig){
            double weight = Double.parseDouble(new String(ch, start, length));
            value.setWeight(weight);
            cWeig = false;
        }
        else if (cCarbs){
            double carb = Double.parseDouble(new String(ch, start, length));
            value.setCarbohydrates(carb);
            cCarbs = false;
        }
        else if (cProt){
            double prot = Double.parseDouble(new String(ch, start, length));
            value.setProtein(prot);
            cProt = false;
        }
        else if (cFat){
            double fat = Double.parseDouble(new String(ch, start, length));
            value.setFat(fat);
            cFat = false;
        }
        else if (cTransFat){
            double trFat = Double.parseDouble(new String(ch, start, length));
            value.setTransFat(trFat);
            cTransFat = false;
        }
        else if (cEnerg){
            int energy = Integer.parseInt(new String(ch, start, length));
            value.setEnergy(energy);
            cEnerg = false;
        }

        else if (cMater){
            cookMethod = new CookMethod();
            String prod = new String(ch, start, length);
            cookMethod.setTechnology(prod);
            cookMethod.setMaterial(new String(ch, start, length));
            value.setCookMethod(cookMethod);
            candie.setValue(value);
            candie.setIngredients(ingredients);
            cMater = false;
        }
    }
}

